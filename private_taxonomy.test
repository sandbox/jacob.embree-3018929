<?php

/**
 * @file
 * Tests for Private Taxonomy.
 */

/**
 * Provides test for Private Taxonomy.
 */
class PrivateTaxonomyWebTestCase extends DrupalWebTestCase {

  /**
   * Test info.
   */
  public static function getInfo() {
    return array(
      'name' => 'Private Taxonomy functionality',
      'description' => 'Test Private Taxonomy functionality.',
      'group' => 'Private Taxonomy',
    );
  }

  /**
   * Enable modules.
   */
  public function setUp() {
    parent::setUp('private_taxonomy');
  }

  /**
   * Test for admin permissions.
   */
  protected function testAdminPrivateTaxonomy() {
    // Create a private vocabulary.
    $data = array(
      'nodes' => array('article' => 'article'),
      'private' => 1,
    );
    $private_vocabulary = $this->createVocabulary($data);
    $data = array(
      'nodes' => array('article' => 'article'),
      'private' => 0,
    );
    $public_vocabulary = $this->createVocabulary($data);

    // Reset the permissions static cache now that new ones exist.
    $this->checkPermissions(array(), TRUE);

    $admin_user = $this->drupalCreateUser(array(
      'access content',
      'administer taxonomy',
    ));
    $user = $this->drupalCreateUser(array(
      'access content',
      'administer own taxonomy',
      'create terms in ' . $private_vocabulary->vid,
      'edit own terms in ' . $private_vocabulary->vid,
      'delete own terms in ' . $private_vocabulary->vid,
    ));
    $this->drupalLogin($admin_user);

    // Test to make sure the vocabulary is private.
    $this->assertEqual(TRUE, private_taxonomy_is_vocabulary_private($private_vocabulary->vid));
    $this->assertEqual(FALSE, private_taxonomy_is_vocabulary_private($public_vocabulary->vid));
    $vocabularies = private_taxonomy_get_private_vocabularies();
    $this->assertEqual(count($vocabularies), 1);
    $this->assertEqual($vocabularies[0]->name, $private_vocabulary->name);

    // Test to see if both vocabularies are visible.
    $this->drupalGet('admin/structure/taxonomy');
    $this->assertText($public_vocabulary->name, t('Public vocabulary visible.'));
    $this->assertText($private_vocabulary->name, t('Private vocabulary visible.'));

    // Add terms to vocabularies.
    $private_term = $this->createTerm($private_vocabulary, array(
      'uid' => $user->uid,
    ));
    $admin_term = $this->createTerm($private_vocabulary, array(
      'uid' => $admin_user->uid,
    ));
    $public_term = $this->createTerm($public_vocabulary);

    // Test to make sure the term is in a private vocabulary.
    $this->assertEqual(TRUE, private_taxonomy_is_term_private($private_term->tid));
    $this->assertEqual(FALSE, private_taxonomy_is_term_private($public_term->tid));

    // Test to retrieve the owner of a term.
    $uid = private_taxonomy_term_get_uid($admin_term->tid);
    $this->assertEqual($admin_user->uid, $uid);
    $this->assertEqual(taxonomy_term_access('view', $admin_term, $admin_user), TRUE);
    $uid = private_taxonomy_term_get_uid($private_term->tid);
    $this->assertEqual($user->uid, $uid);
    $uid = private_taxonomy_term_get_uid($public_term->tid);
    $this->assertEqual(FALSE, $uid);

    // Test to see what terms are visible.
    $this->drupalGet('admin/structure/taxonomy/' . $private_vocabulary->machine_name);
    $this->assertText($admin_term->name, t('Admin private term visible.'));
    $this->assertText($private_term->name, t('User private term visible.'));
    $this->drupalGet('admin/structure/taxonomy/' . $public_vocabulary->machine_name);
    $this->assertText($public_term->name, t('Public term visible.'));
  }

  /**
   * Test for user with 'administer own taxonomy' permission.
   */
  protected function testUserPrivateTaxonomy() {
    // Create a private vocabulary.
    $data = array(
      'nodes' => array('article' => 'article'),
      'private' => 1,
    );
    $private_vocabulary = $this->createVocabulary($data);
    $data = array(
      'nodes' => array('article' => 'article'),
      'private' => 0,
    );
    $public_vocabulary = $this->createVocabulary($data);

    // Reset the permissions static cache now that new ones exist.
    $this->checkPermissions(array(), TRUE);

    $admin_user = $this->drupalCreateUser(array(
      'access content',
      'administer taxonomy',
    ));
    $user = $this->drupalCreateUser(array(
      'access content',
      'administer own taxonomy',
      'create terms in ' . $private_vocabulary->vid,
      'edit own terms in ' . $private_vocabulary->vid,
      'delete own terms in ' . $private_vocabulary->vid,
    ));
    $this->drupalLogin($user);

    // Add terms to vocabularies.
    $private_term = $this->createTerm($private_vocabulary, array(
      'uid' => $user->uid,
    ));
    $admin_term = $this->createTerm($private_vocabulary, array(
      'uid' => $admin_user->uid,
    ));

    // Test to see what vocabularies are visible.
    $this->drupalGet('admin/structure/taxonomy');
    $this->assertNoText($public_vocabulary->name, t('Public vocabulary not visible.'));
    $this->assertText($private_vocabulary->name, t('Private vocabulary visible.'));

    // Test to see what terms are visible.
    $this->drupalGet('admin/structure/taxonomy/' . $private_vocabulary->machine_name);
    $this->assertText($private_term->name, t('Private term visible.'));
    $this->assertNoText($admin_term->name, t('Admin term not visible.'));
  }

  /**
   * Test for user with 'view private taxonomies' permission.
   *
   * Uses 'all' for widget.
   */
  protected function testViewPrivateTaxonomyAll() {
    $admin_user = $this->drupalCreateUser(array(
      'access content',
      'administer taxonomy',
      'administer content types',
    ));
    $user = $this->drupalCreateUser(array(
      'access content',
      'view private taxonomies',
      'create article content',
      'create page content',
      'edit own page content',
    ));

    // Create a private vocabulary.
    $data = array('private' => 1);
    $private_vocabulary = $this->createVocabulary($data);
    $data = array('private' => 0);
    $public_vocabulary = $this->createVocabulary($data);

    // Add terms to vocabularies.
    $private_term = $this->createTerm($private_vocabulary, array(
      'uid' => $user->uid,
    ));
    $admin_term = $this->createTerm($private_vocabulary, array(
      'uid' => $admin_user->uid,
    ));

    $this->drupalLogin($user);

    // Test to see what vocabularies are visible.
    $this->drupalGet('admin/structure/taxonomy');
    $this->assertNoText($public_vocabulary->name, t('Public vocabulary not visible.'));
    $this->assertText($private_vocabulary->name, t('Private vocabulary visible.'));

    // Test to see what terms are visible.
    $this->drupalGet('admin/structure/taxonomy/' . $private_vocabulary->machine_name . '/view');
    $this->assertText($private_term->name, t('Private term visible.'));
    $this->assertText($admin_term->name, t('Admin term visible.'));

    $data = array('field_name' => 'field_private');
    $field = $this->createField($private_vocabulary, $data);
    $data = array(
      'bundle' => 'page',
      'widget' => array(
        'type' => 'private_taxonomy_widget_autocomplete',
      ),
      'display' => array(
        'default' => array(
          'type' => 'private_taxonomy_term_reference_link',
        ),
      ),
    );
    $instance = $this->createInstance($field, $data);

    $edit = array(
      'title' => $this->randomName(),
      'field_private[und]' => $admin_term->name,
    );
    $this->drupalPost('node/add/page', $edit, t('Save'));
    // Should find the owner's term and use it.
    $this->assertRaw('taxonomy/term/' . $admin_term->tid, t('Found term'));
    // Check taxonomy index.
    $this->drupalGet('taxonomy/term/' . $admin_term->tid);
    $this->assertRaw($admin_term->name, t('Found term'));
  }

  /**
   * Test for user with 'view private taxonomies' permission.
   *
   * Uses 'owner' for widget.
   */
  protected function testViewPrivateTaxonomyOwner() {
    $admin_user = $this->drupalCreateUser(array(
      'access content',
      'administer taxonomy',
      'administer content types',
    ));
    $user = $this->drupalCreateUser(array(
      'access content',
      'view private taxonomies',
      'create article content',
      'create page content',
      'edit own page content',
    ));

    // Create a private vocabulary.
    $data = array('private' => 1);
    $private_vocabulary = $this->createVocabulary($data);
    $data = array('private' => 0);
    $public_vocabulary = $this->createVocabulary($data);

    // Add terms to vocabularies.
    $private_term = $this->createTerm($private_vocabulary, array(
      'uid' => $user->uid,
    ));
    $admin_term = $this->createTerm($private_vocabulary, array(
      'uid' => $admin_user->uid,
    ));

    $this->drupalLogin($user);

    // Test to see what vocabularies are visible.
    $this->drupalGet('admin/structure/taxonomy');
    $this->assertNoText($public_vocabulary->name, t('Public vocabulary not visible.'));
    $this->assertText($private_vocabulary->name, t('Private vocabulary visible.'));

    // Test to see what terms are visible.
    $this->drupalGet('admin/structure/taxonomy/' . $private_vocabulary->machine_name . '/view');
    $this->assertText($private_term->name, t('Private term visible.'));
    $this->assertText($admin_term->name, t('Admin term visible.'));

    $data = array(
      'field_name' => 'field_private',
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => $private_vocabulary->machine_name,
            'users' => 'owner',
          ),
        ),
      ),
    );
    $field = $this->createField($private_vocabulary, $data);
    $data = array(
      'bundle' => 'page',
      'widget' => array(
        'type' => 'private_taxonomy_widget_autocomplete',
      ),
      'display' => array(
        'default' => array(
          'type' => 'private_taxonomy_term_reference_link',
        ),
      ),
    );
    $instance = $this->createInstance($field, $data);

    $edit = array(
      'title' => $this->randomName(),
      'field_private[und]' => $admin_term->name,
    );
    $this->drupalPost('node/add/page', $edit, t('Save'));
    // Should not use the admin's term but create a new one.
    $this->assertNoRaw('taxonomy/term/' . $admin_term->tid, t('Created new term'));
    // Another check.
    $this->drupalGet('taxonomy/term/' . $admin_term->tid);
    $this->assertText('There is currently no content classified with this term', t('Created new term'));
  }

  /**
   * Test for user with both permissions.
   */
  protected function testBothPrivateTaxonomy() {
    $admin_user = $this->drupalCreateUser(array('administer taxonomy'));
    $user = $this->drupalCreateUser(array(
      'access content',
      'administer own taxonomy',
      'view private taxonomies',
    ));

    // Create a private vocabulary.
    $data = array(
      'nodes' => array('article' => 'article'),
      'private' => 1,
    );
    $private_vocabulary = $this->createVocabulary($data);
    $data = array(
      'nodes' => array('article' => 'article'),
      'private' => 0,
    );
    $public_vocabulary = $this->createVocabulary($data);

    // Add terms to vocabularies.
    $private_term = $this->createTerm($private_vocabulary, array(
      'uid' => $user->uid,
    ));
    $admin_term = $this->createTerm($private_vocabulary, array(
      'uid' => $admin_user->uid,
    ));

    $this->drupalLogin($user);

    // Test to see what vocabularies are visible.
    $this->drupalGet('admin/structure/taxonomy');
    $this->assertNoText($public_vocabulary->name, t('Public vocabulary not visible.'));
    $this->assertText($private_vocabulary->name, t('Private vocabulary visible.'));

    // Test to see what terms are visible.
    $this->drupalGet('admin/structure/taxonomy/' . $private_vocabulary->machine_name);
    $this->assertText($private_term->name, t('Private term visible.'));
    // Old version would not show accessible terms here unless user has
    // 'administer taxonomy' permission.  Other terms would be at
    // /admin/structure/taxonomy/%taxonomy_vocabulary/view.
    $this->assertText($admin_term->name, t('Admin term visible.'));
  }

  /**
   * Returns a new vocabulary with random properties.
   */
  protected function createVocabulary($data = array()) {
    // Create a vocabulary.
    $vocabulary = $data + array(
      'name' => $this->randomName(),
      'description' => $this->randomName(),
      'machine_name' => drupal_strtolower($this->randomName()),
      'help' => '',
      'nodes' => array(),
      'weight' => mt_rand(0, 10),
      'private' => mt_rand(0, 1),
    );
    $vocabulary = (object) $vocabulary;
    taxonomy_vocabulary_save($vocabulary);
    return $vocabulary;
  }

  /**
   * Returns a new term with random properties in vocabulary $vid.
   */
  protected function createTerm($vocabulary, $data = array()) {
    $term = $data + array(
      'name' => $this->randomName(),
      'description' => $this->randomName(),
      // Use the first available text format.
      'format' => db_query_range('SELECT format FROM {filter_format}', 0, 1)->fetchField(),
      'vid' => $vocabulary->vid,
    );
    $term = (object) $term;
    taxonomy_term_save($term);
    return $term;
  }

  /**
   * Returns a new field with random properties for vocabulary $vid.
   */
  protected function createField($vocabulary, $data = array()) {
    $field = $data + array(
      'field_name' => 'field_' . drupal_strtolower($this->randomName()),
      'type' => 'private_taxonomy_term_reference',
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => $vocabulary->machine_name,
            'users' => 'all',
          ),
        ),
      ),
    );
    field_create_field($field);
    return $field;
  }

  /**
   * Returns a new field instance with random properties for vocabulary $vid.
   */
  protected function createInstance($field, $data = array()) {
    $instance = $data + array(
      'field_name' => $field['field_name'],
      'entity_type' => 'node',
      'label' => $this->randomName(),
      'bundle' => '',
      'required' => mt_rand(0, 1),
      'widget' => array('type' => 'options_select'),
      'display' => array(
        'default' => array('type' => 'hidden'),
        'teaser' => array('type' => 'hidden'),
      ),
    );
    field_create_instance($instance);
    return $instance;
  }

}
